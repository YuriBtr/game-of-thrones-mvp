package ru.skillbranch.yuribtr.ui.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import java.util.List;

import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.utils.ConstantManager;
import ru.skillbranch.yuribtr.utils.App;

public class CharactersAdapter extends RecyclerView.Adapter<CharactersAdapter.CharacterViewHolder> {
    private static final String TAG = ConstantManager.TAG_PREFIX + "UsersAdapter";
    private Context mContext;
    private List<Character> mCharacterList;
    private DaoSession mDaoSession;

    private final OnItemClickListener mListener;

    public interface OnItemClickListener {
        void onItemClick(Character item);
    }


    public CharactersAdapter(List<Character> characterList, OnItemClickListener listener) {
        mCharacterList = characterList;
        this.mListener = listener;
        mDaoSession = App.getDaoSession();
        mContext = App.getContext();
        //UiHelper.writeLog("CharactersAdapter constructor");
    }

    @Override
    public CharacterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        //UiHelper.writeLog("onCreateViewHolder");
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.item_character_list, parent, false);
        return new CharacterViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(CharacterViewHolder holder, int position) {
        Character character = mCharacterList.get(position);
        holder.bind(mCharacterList.get(position), mListener);

        holder.mName.setText(character.getName());
        if (character.getTitles()==null || character.getTitles().isEmpty()) holder.mTitle.setText(character.getAliases());
        else holder.mTitle.setText(character.getTitles());
        if (!character.getDied().isEmpty())
            holder.mDied.setImageDrawable(mContext.getResources().getDrawable( R.drawable.skull));

        //dynamically assign an icon basing on house name
        if ("stark".equalsIgnoreCase(character.getHouseShortName()))
            holder.mPhoto.setImageDrawable(mContext.getResources().getDrawable( R.drawable.ic_stark_icon));
        else if ("lannister".equalsIgnoreCase(character.getHouseShortName()))
            holder.mPhoto.setImageDrawable(mContext.getResources().getDrawable( R.drawable.ic_lanister_icon));
        else if ("targaryen".equalsIgnoreCase(character.getHouseShortName()))
            holder.mPhoto.setImageDrawable(mContext.getResources().getDrawable( R.drawable.ic_targariyen_icon));
    }

    @Override
    public int getItemCount() {
        return mCharacterList.size();
    }


    public class CharacterViewHolder extends RecyclerView.ViewHolder{
        private ImageView mPhoto, mDied;
        private TextView mName, mTitle;


        public CharacterViewHolder(View itemView) {
            super(itemView);
            mPhoto = (ImageView) itemView.findViewById(R.id.character_photo_iv);
            mDied = (ImageView) itemView.findViewById(R.id.character_died_iv);
            mName = (TextView) itemView.findViewById(R.id.character_name_tv);
            mTitle = (TextView) itemView.findViewById(R.id.character_title_tv);
        }

        public void bind(final Character item, final OnItemClickListener listener) {

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.onItemClick(item);
                }
            });
        }

    }

}
package ru.skillbranch.yuribtr.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.data.storage.models.CharacterDTO;
import ru.skillbranch.yuribtr.utils.ConstantManager;
import ru.skillbranch.yuribtr.utils.SquareImageView;

public class DetailedActivity extends BaseActivity {
    private CoordinatorLayout mCoordinatorLayout;
    private CharacterDTO mCharacterDTO;

    @BindView(R.id.character_image_iv)
    SquareImageView mSquareImageView;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout mCollapsingToolbarLayout;

    @BindView(R.id.character_father_btn)
    Button mCharFather;

    @BindView(R.id.character_mother_btn)
    Button mCharMother;

    @BindView(R.id.character_father_ll)
    LinearLayout mCharFatherLL;

    @BindView(R.id.character_mother_ll)
    LinearLayout mCharMotherLL;

    @BindView(R.id.character_words_tv)
    TextView mCharWords;

    @BindView(R.id.character_born_tv)
    TextView mCharBorn;

    @BindView(R.id.character_titles_tv)
    TextView mCharTitles;

    @BindView(R.id.character_aliases_tv)
    TextView mCharAliases;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailed);
        ButterKnife.bind(this);

        mSquareImageView = (SquareImageView) findViewById(R.id.character_image_iv);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mCollapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator_container);

        mCharWords = (TextView) findViewById(R.id.character_words_tv);
        mCharBorn = (TextView) findViewById(R.id.character_born_tv);
        mCharTitles = (TextView) findViewById(R.id.character_titles_tv);
        mCharAliases = (TextView) findViewById(R.id.character_aliases_tv);
        mCharFather = (Button) findViewById(R.id.character_father_btn);
        mCharMother = (Button) findViewById(R.id.character_mother_btn);
        mCharFatherLL = (LinearLayout) findViewById(R.id.character_father_ll);
        mCharMotherLL = (LinearLayout) findViewById(R.id.character_mother_ll);

        setupToolbar();
        initCharacterData();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    private void initCharacterData(){
        //parsing EXTRA info from another activity
        mCharacterDTO = getIntent().getParcelableExtra(ConstantManager.PARCELABLE_KEY);
        if (mCharacterDTO!=null) {

            mCharWords.setText(mCharacterDTO.getHouseWords());
            mCharBorn.setText(mCharacterDTO.getBorn());
            mCharTitles.setText(mCharacterDTO.getTitles());
            mCharAliases.setText(mCharacterDTO.getAliases());
            mCollapsingToolbarLayout.setTitle(mCharacterDTO.getName());

            if (mCharacterDTO.getFatherCharacter()!=null) {
                mCharFather.setText(mCharacterDTO.getFatherCharacter().getName());
                mCharFather.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(DetailedActivity.this, DetailedActivity.class);
                        //as we don't know exact house of parent, let's assume he is from same house as child
                        mCharacterDTO.getFatherCharacter().setHouseShortName(mCharacterDTO.getHouseShortName());
                        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, mCharacterDTO.getFatherCharacter());
                        startActivity(profileIntent);
                        finish();
                    }
                });
                mCharFatherLL.setVisibility(View.VISIBLE);
            } else mCharFatherLL.setVisibility(View.INVISIBLE);

            if (mCharacterDTO.getMotherCharacter()!=null) {
                mCharMother.setText(mCharacterDTO.getMotherCharacter().getName());
                mCharMother.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent profileIntent = new Intent(DetailedActivity.this, DetailedActivity.class);
                        //as we don't know exact house of parent, let's assume he is from same house as child
                        mCharacterDTO.getFatherCharacter().setHouseShortName(mCharacterDTO.getHouseShortName());
                        mCharacterDTO.getMotherCharacter().setHouseShortName(mCharacterDTO.getHouseShortName());
                        profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, mCharacterDTO.getMotherCharacter());
                        startActivity(profileIntent);
                        finish();
                    }
                });
                mCharMotherLL.setVisibility(View.VISIBLE);
            } else mCharMotherLL.setVisibility(View.INVISIBLE);

            if ("stark".equalsIgnoreCase(mCharacterDTO.getHouseShortName()))
                mSquareImageView.setImageDrawable(getResources().getDrawable( R.drawable.stark));
            else if ("lannister".equalsIgnoreCase(mCharacterDTO.getHouseShortName()))
                mSquareImageView.setImageDrawable(getResources().getDrawable( R.drawable.lannister));
            else if ("targaryen".equalsIgnoreCase(mCharacterDTO.getHouseShortName()))
                mSquareImageView.setImageDrawable(getResources().getDrawable( R.drawable.targarien));

            String[] tvSeries = mCharacterDTO.getTvSeries().split(ConstantManager.URL_DELIMITER);
            String lastSeason=null;
            if (tvSeries.length>0 && !tvSeries[tvSeries.length-1].isEmpty())
                lastSeason = ConstantManager.URL_DELIMITER+getString(R.string.last_season)+": "+tvSeries[tvSeries.length-1];
            else
                lastSeason = ConstantManager.URL_DELIMITER+getString(R.string.last_season)+": "+getString(R.string.season_unknown);

            if (mCharacterDTO.getDied()!=null && !mCharacterDTO.getDied().isEmpty()) {
                if ("Male".equalsIgnoreCase(mCharacterDTO.getGender()))
                    showSnackbar(mCharacterDTO.getName()+" "+getString(R.string.male_died)+": "+mCharacterDTO.getDied()+lastSeason);
                else
                    showSnackbar(mCharacterDTO.getName()+" "+getString(R.string.female_died)+": "+mCharacterDTO.getDied()+lastSeason);
            }

        }

    }

    private void showSnackbar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}

package ru.skillbranch.yuribtr.data.storage.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToOne;
import org.greenrobot.greendao.annotation.Unique;

@Entity(active = true, nameInDb = "CHARACTERS")
public class Character {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Long remoteId;

    private Long houseId;

    private String houseShortName;

    private String url;

    private String name;

    private String gender;

    private String culture;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private String father;

    private String mother;

    private Long fatherId;
    
    private Long motherId;

    @ToOne(joinProperty = "fatherId")
    private Parent fatherPerson;

    @ToOne(joinProperty = "motherId")
    private Parent motherPerson;

    private String spouse;

    private String allegiances;

    private String books;

    private String povBooks;

    private String tvSeries;

    private String playedBy;

    private String houseWords;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    /** Used for active entity operations. */
    @Generated(hash = 898307126)
    private transient CharacterDao myDao;


    @Generated(hash = 149554501)
    public Character(Long id, @NotNull Long remoteId, Long houseId, String houseShortName,
            String url, String name, String gender, String culture, String born, String died,
            String titles, String aliases, String father, String mother, Long fatherId,
            Long motherId, String spouse, String allegiances, String books, String povBooks,
            String tvSeries, String playedBy, String houseWords) {
        this.id = id;
        this.remoteId = remoteId;
        this.houseId = houseId;
        this.houseShortName = houseShortName;
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.fatherId = fatherId;
        this.motherId = motherId;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
        this.houseWords = houseWords;
    }

    @Generated(hash = 1853959157)
    public Character() {
    }

    @Generated(hash = 1561865449)
    private transient Long fatherPerson__resolvedKey;

    @Generated(hash = 710136505)
    private transient Long motherPerson__resolvedKey;


    public CharacterDTO convertToCharacterDTO () {

        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setId(getId());
        characterDTO.setRemoteId(getRemoteId());
        characterDTO.setHouseId(getHouseId());
        characterDTO.setHouseShortName(getHouseShortName());
        characterDTO.setUrl(getUrl());
        characterDTO.setName(getName());
        characterDTO.setGender(getGender());
        characterDTO.setCulture(getCulture());
        characterDTO.setBorn(getBorn());
        characterDTO.setDied(getDied());
        characterDTO.setTitles(getTitles());
        characterDTO.setAliases(getAliases());
        characterDTO.setFather(getFather());
        characterDTO.setFatherId(getFatherId());
        Parent tmpFather = getFatherPerson();
        if (tmpFather!=null) characterDTO.setFatherCharacter(getFatherPerson().convertToCharacterDTO());
        Parent tmpMother = getMotherPerson();
        if (tmpMother!=null) characterDTO.setMotherCharacter(getMotherPerson().convertToCharacterDTO());
        characterDTO.setMother(getMother());
        characterDTO.setMotherId(getMotherId());
        characterDTO.setSpouse(getSpouse());
        characterDTO.setAllegiances(getAllegiances());
        characterDTO.setBooks(getBooks());
        characterDTO.setPovBooks(getPovBooks());
        characterDTO.setTvSeries(getTvSeries());
        characterDTO.setPlayedBy(getPlayedBy());
        characterDTO.setHouseWords(getHouseWords());
        return characterDTO;

    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getHouseShortName() {
        return houseShortName;
    }

    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getAliases() {
        return aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public Long getFatherId() {
        return fatherId;
    }

    public void setFatherId(Long fatherId) {
        this.fatherId = fatherId;
    }

    public Long getMotherId() {
        return motherId;
    }

    public void setMotherId(Long motherId) {
        this.motherId = motherId;
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 2096968813)
    public Parent getFatherPerson() {
        Long __key = this.fatherId;
        if (fatherPerson__resolvedKey == null || !fatherPerson__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ParentDao targetDao = daoSession.getParentDao();
            Parent fatherPersonNew = targetDao.load(__key);
            synchronized (this) {
                fatherPerson = fatherPersonNew;
                fatherPerson__resolvedKey = __key;
            }
        }
        return fatherPerson;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1196713724)
    public void setFatherPerson(Parent fatherPerson) {
        synchronized (this) {
            this.fatherPerson = fatherPerson;
            fatherId = fatherPerson == null ? null : fatherPerson.getId();
            fatherPerson__resolvedKey = fatherId;
        }
    }

    /** To-one relationship, resolved on first access. */
    @Generated(hash = 843927598)
    public Parent getMotherPerson() {
        Long __key = this.motherId;
        if (motherPerson__resolvedKey == null || !motherPerson__resolvedKey.equals(__key)) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            ParentDao targetDao = daoSession.getParentDao();
            Parent motherPersonNew = targetDao.load(__key);
            synchronized (this) {
                motherPerson = motherPersonNew;
                motherPerson__resolvedKey = __key;
            }
        }
        return motherPerson;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 625576880)
    public void setMotherPerson(Parent motherPerson) {
        synchronized (this) {
            this.motherPerson = motherPerson;
            motherId = motherPerson == null ? null : motherPerson.getId();
            motherPerson__resolvedKey = motherId;
        }
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getAllegiances() {
        return allegiances;
    }

    public void setAllegiances(String allegiances) {
        this.allegiances = allegiances;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public String getPovBooks() {
        return povBooks;
    }

    public void setPovBooks(String povBooks) {
        this.povBooks = povBooks;
    }

    public String getTvSeries() {
        return tvSeries;
    }

    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    public String getPlayedBy() {
        return playedBy;
    }

    public void setPlayedBy(String playedBy) {
        this.playedBy = playedBy;
    }

    public String getHouseWords() {
        return houseWords;
    }

    public void setHouseWords(String houseWords) {
        this.houseWords = houseWords;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 162219484)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getCharacterDao() : null;
    }
}


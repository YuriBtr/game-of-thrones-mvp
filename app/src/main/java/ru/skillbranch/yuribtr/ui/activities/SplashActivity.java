package ru.skillbranch.yuribtr.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.mvp.presenters.SplashPresenter;
import ru.skillbranch.yuribtr.mvp.views.ISplashView;

public class SplashActivity extends BaseActivity implements ISplashView {
    SplashPresenter mPresenter = SplashPresenter.getInstance();

    @BindView(R.id.splash_message)
    TextView mSplashMessage;

    //region=================Lifecycle=================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        mPresenter.takeView(this);
        mPresenter.initView();
    }

    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }
    //endregion

    //region=================INTERFACE METHODS=================
    @Override
    public void showMessage(String message) {
        mSplashMessage.setText(message);
    }

    @Override
    public void showError(Throwable e) {
        mSplashMessage.setText(e.getMessage());
    }

    @Override
    public void showLoading() {
        showProgress();
    }

    @Override
    public void hideLoading() {
        hideProgress();
    }

    @Override
    public void gotoNextActivity() {
        Intent intent = new Intent(SplashActivity.this, RootActivity.class);
        startActivity(intent);
        finish();
    }
    //endregion
}

package ru.skillbranch.yuribtr.mvp.presenters;

import android.content.Context;
import android.support.annotation.Nullable;

import java.util.List;

import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.mvp.models.ILoadModel;
import ru.skillbranch.yuribtr.mvp.models.LoadDbModel;
import ru.skillbranch.yuribtr.mvp.models.LoadNetworkModel;
import ru.skillbranch.yuribtr.ui.activities.DetailedActivity;
import ru.skillbranch.yuribtr.mvp.views.IRootView;
import ru.skillbranch.yuribtr.ui.adapters.CharactersAdapter;

public class RootPresenter implements IRootPresenter, INotifierPresenter {
    private static RootPresenter ourInstance = new RootPresenter();
    private IRootView mRootView;
    private ILoadModel mLoadModel;
    private Context mContext;
    private LoadDbModel mLoadDbModel;
    private List<House> mHouseList;

    //Singleton
    public static RootPresenter getInstance(){
        return ourInstance;
    }

    private RootPresenter() {
        mLoadModel = new LoadNetworkModel(this);
        mContext = mLoadModel.getContext();
        mLoadDbModel = new LoadDbModel(this);
    }

    @Override
    public void onLoadNetworkFinished() {
        if (getView()!=null) getView().showMessage(mContext.getString(R.string.network_data_recieved_mvp));
    }

    @Override
    public void takeView(IRootView rootView) {
        mRootView = rootView;
    }

    @Override
    public void dropView() {
        mRootView = null;
    }

    @Override
    public void initView() {
        mHouseList = mLoadDbModel.getHouses();
        if (getView()!=null && mHouseList.size()>=3) {
            getView().assignTabsNames(
                    mHouseList.get(0).getHouseShortName(),
                    mHouseList.get(1).getHouseShortName(),
                    mHouseList.get(2).getHouseShortName());
        }
    }

    @Override
    public void onPageSelected(int tabPosition) {
        //simple check for index out of range
        if (mHouseList!=null && tabPosition<mHouseList.size() && getView()!=null) {
            getView().createAdapter(mHouseList.get(tabPosition).getSwornMembersList(), DetailedActivity.class);
        }
    }

    @Nullable
    @Override
    public IRootView getView() {
        return mRootView;
    }

    @Override
    public void onLoadProgress(String message) {
        if (getView()!=null) getView().showMessage(message);
    }
}

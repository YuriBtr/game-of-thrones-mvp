package ru.skillbranch.yuribtr.utils;

public interface ConstantManager {

    int STATE_IDLE = 0;
    int STATE_NETWORK_LOADING = 1;
    int STATE_DB_LOADING = 2;

    String TAG_PREFIX="DEV ";
    boolean DEBUG_MODE = true;

    String LAST_REFRESH = "LAST_REFRESH";

    String PARCELABLE_KEY = "PARCELABLE_KEY";
    int CACHE_EXPIRE_DEFAULT = 86400;

    String URL_DELIMITER = ", ";

}

package ru.skillbranch.yuribtr.data.managers;

import android.content.Context;

import ru.skillbranch.yuribtr.data.network.RestService;
import ru.skillbranch.yuribtr.data.network.ServiceGenerator;
import ru.skillbranch.yuribtr.data.network.res.CharacterRes;
import ru.skillbranch.yuribtr.data.network.res.HouseRes;
import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.utils.App;

import retrofit2.Call;

public class DataManager {
    private static DataManager INSTANCE = null;
    private Context mContext;
    private PreferencesManager mPreferencesManager;
    private RestService mRestService;

    private DaoSession mDaoSession;

    public DataManager(){
        this.mPreferencesManager = new PreferencesManager();
        this.mContext = App.getContext();
        this.mRestService = ServiceGenerator.createService(RestService.class);
        this.mDaoSession = App.getDaoSession();
    }

    public static DataManager getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DataManager();
        }
        return INSTANCE;
    }

    public DataManager(PreferencesManager preferencesManager) {mPreferencesManager = preferencesManager;}

    public PreferencesManager getPreferencesManager() {return mPreferencesManager;}

    public Context getContext(){return mContext;}

    //region ============ NETWORK ============


    public Call<HouseRes> getHouse (Long houseId){return mRestService.getHouse(houseId);}

    public Call<CharacterRes> getCharacter(int characterId){return mRestService.getCharacter(characterId);}

    //region ============ DATABASE ============

    public DaoSession getDaoSession() {return mDaoSession;}






    //endregion
}
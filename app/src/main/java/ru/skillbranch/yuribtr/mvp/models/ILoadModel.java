package ru.skillbranch.yuribtr.mvp.models;

import android.content.Context;

public interface ILoadModel {
    void LoadFromNetwork();
    Context getContext();
}

package ru.skillbranch.yuribtr.mvp.presenters;

import android.support.annotation.Nullable;

import ru.skillbranch.yuribtr.mvp.views.ISplashView;

public interface ISplashPresenter {
    void takeView(ISplashView splashView);
    void dropView();
    void initView();

    void onLoadNetworkFinished();

    void onLoadProgress(String message);

    @Nullable
    ISplashView getView();
}

package ru.skillbranch.yuribtr.data.storage.models;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;

@Entity(active = true, nameInDb = "PARENTS")
public class Parent {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Long remoteId;

    private Long houseId;

    private String houseShortName;

    private String url;

    private String name;

    private String gender;

    private String culture;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private String father;

    private String mother;

    private Long fatherId;

    private Long motherId;

    private String spouse;

    private String allegiances;

    private String books;

    private String povBooks;

    private String tvSeries;

    private String playedBy;

    private String houseWords;

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1321580708)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getParentDao() : null;
    }

    /** Used for active entity operations. */
    @Generated(hash = 936059064)
    private transient ParentDao myDao;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    public String getHouseWords() {
        return this.houseWords;
    }

    public void setHouseWords(String houseWords) {
        this.houseWords = houseWords;
    }

    public String getPlayedBy() {
        return this.playedBy;
    }

    public void setPlayedBy(String playedBy) {
        this.playedBy = playedBy;
    }

    public String getTvSeries() {
        return this.tvSeries;
    }

    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    public String getPovBooks() {
        return this.povBooks;
    }

    public void setPovBooks(String povBooks) {
        this.povBooks = povBooks;
    }

    public String getBooks() {
        return this.books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public String getAllegiances() {
        return this.allegiances;
    }

    public void setAllegiances(String allegiances) {
        this.allegiances = allegiances;
    }

    public String getSpouse() {
        return this.spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public Long getMotherId() {
        return this.motherId;
    }

    public void setMotherId(Long motherId) {
        this.motherId = motherId;
    }

    public Long getFatherId() {
        return this.fatherId;
    }

    public void setFatherId(Long fatherId) {
        this.fatherId = fatherId;
    }

    public String getMother() {
        return this.mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public String getFather() {
        return this.father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getAliases() {
        return this.aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getTitles() {
        return this.titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getDied() {
        return this.died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getBorn() {
        return this.born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getCulture() {
        return this.culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getGender() {
        return this.gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return this.url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHouseShortName() {
        return this.houseShortName;
    }

    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public Long getHouseId() {
        return this.houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public Long getRemoteId() {
        return this.remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Generated(hash = 831439041)
    public Parent(Long id, @NotNull Long remoteId, Long houseId,
            String houseShortName, String url, String name, String gender,
            String culture, String born, String died, String titles,
            String aliases, String father, String mother, Long fatherId,
            Long motherId, String spouse, String allegiances, String books,
            String povBooks, String tvSeries, String playedBy, String houseWords) {
        this.id = id;
        this.remoteId = remoteId;
        this.houseId = houseId;
        this.houseShortName = houseShortName;
        this.url = url;
        this.name = name;
        this.gender = gender;
        this.culture = culture;
        this.born = born;
        this.died = died;
        this.titles = titles;
        this.aliases = aliases;
        this.father = father;
        this.mother = mother;
        this.fatherId = fatherId;
        this.motherId = motherId;
        this.spouse = spouse;
        this.allegiances = allegiances;
        this.books = books;
        this.povBooks = povBooks;
        this.tvSeries = tvSeries;
        this.playedBy = playedBy;
        this.houseWords = houseWords;
    }

    @Generated(hash = 981245553)
    public Parent() {
    }

    public CharacterDTO convertToCharacterDTO() {

        CharacterDTO characterDTO = new CharacterDTO();
        characterDTO.setId(getId());
        characterDTO.setRemoteId(getRemoteId());
        characterDTO.setHouseId(getHouseId());
        characterDTO.setHouseShortName(getHouseShortName());
        characterDTO.setUrl(getUrl());
        characterDTO.setName(getName());
        characterDTO.setGender(getGender());
        characterDTO.setCulture(getCulture());
        characterDTO.setBorn(getBorn());
        characterDTO.setDied(getDied());
        characterDTO.setTitles(getTitles());
        characterDTO.setAliases(getAliases());
        characterDTO.setFather(getFather());
        characterDTO.setFatherId(getFatherId());
        characterDTO.setMother(getMother());
        characterDTO.setMotherId(getMotherId());
        characterDTO.setFatherCharacter(null);
        characterDTO.setMotherCharacter(null);
        characterDTO.setSpouse(getSpouse());
        characterDTO.setAllegiances(getAllegiances());
        characterDTO.setBooks(getBooks());
        characterDTO.setPovBooks(getPovBooks());
        characterDTO.setTvSeries(getTvSeries());
        characterDTO.setPlayedBy(getPlayedBy());
        characterDTO.setHouseWords(getHouseWords());
        return characterDTO;

    }


}


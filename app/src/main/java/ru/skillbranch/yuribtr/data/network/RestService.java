package ru.skillbranch.yuribtr.data.network;

import ru.skillbranch.yuribtr.data.network.res.CharacterRes;
import ru.skillbranch.yuribtr.data.network.res.HouseRes;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface RestService {

    @GET("houses/{houseId}")
    Call<HouseRes> getHouse (@Path("houseId") Long houseId);

    @GET("characters/{characterId}")
    Call<CharacterRes> getCharacter (@Path("characterId") int characterId);


}

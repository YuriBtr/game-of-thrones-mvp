package ru.skillbranch.yuribtr.mvp.views;

public interface ISplashView {
    void showMessage (String message);
    void showError (Throwable e);
    void showLoading ();
    void hideLoading ();
    void gotoNextActivity();
}

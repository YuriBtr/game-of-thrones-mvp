package ru.skillbranch.yuribtr.mvp.presenters;

import android.support.annotation.Nullable;
import ru.skillbranch.yuribtr.mvp.views.IRootView;
import ru.skillbranch.yuribtr.ui.adapters.CharactersAdapter;

public interface IRootPresenter {
    void takeView(IRootView rootView);
    void dropView();
    void initView();

    void onPageSelected(int tabPosition);


    @Nullable
    IRootView getView();
}

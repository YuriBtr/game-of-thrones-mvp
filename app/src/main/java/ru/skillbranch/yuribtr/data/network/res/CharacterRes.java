package ru.skillbranch.yuribtr.data.network.res;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.data.storage.models.Parent;
import ru.skillbranch.yuribtr.utils.ConstantManager;


public class CharacterRes {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("culture")
    @Expose
    private String culture;
    @SerializedName("born")
    @Expose
    private String born;
    @SerializedName("died")
    @Expose
    private String died;
    @SerializedName("titles")
    @Expose
    private List<String> titles = new ArrayList<>();
    @SerializedName("aliases")
    @Expose
    private List<String> aliases = new ArrayList<>();
    @SerializedName("father")
    @Expose
    private String father;
    @SerializedName("mother")
    @Expose
    private String mother;
    @SerializedName("spouse")
    @Expose
    private String spouse;
    @SerializedName("allegiances")
    @Expose
    private List<String> allegiances = new ArrayList<>();
    @SerializedName("books")
    @Expose
    private List<String> books = new ArrayList<>();
    @SerializedName("povBooks")
    @Expose
    private List<String> povBooks = new ArrayList<>();
    @SerializedName("tvSeries")
    @Expose
    private List<String> tvSeries = new ArrayList<>();
    @SerializedName("playedBy")
    @Expose
    private List<String> playedBy = new ArrayList<>();

    public Character convertToCharacter () {

        Character character = new Character();

        character.setUrl(getUrl());
        character.setRemoteId(extractId(getUrl()));
        character.setHouseId(0L);
        character.setName(getName());
        character.setGender(getGender());
        character.setCulture(getCulture());
        character.setBorn(getBorn());
        character.setDied(getDied());
        character.setFather(getFather());
        character.setMother(getMother());
        character.setSpouse(getSpouse());
        if (!getFather().isEmpty()) character.setFatherId(extractId(getFather()));
        if (!getMother().isEmpty()) character.setMotherId(extractId(getMother()));

        //due to problem with saving List<String> at greendao, we concatinate all strings together at same field
        character.setTitles(TextUtils.join(ConstantManager.URL_DELIMITER, getTitles()));
        character.setAliases(TextUtils.join(ConstantManager.URL_DELIMITER, getAliases()));
        character.setAllegiances(TextUtils.join(ConstantManager.URL_DELIMITER, getAllegiances()));
        character.setBooks(TextUtils.join(ConstantManager.URL_DELIMITER, getBooks()));
        character.setPovBooks(TextUtils.join(ConstantManager.URL_DELIMITER, getPovBooks()));
        character.setTvSeries(TextUtils.join(ConstantManager.URL_DELIMITER, getTvSeries()));
        character.setPlayedBy(TextUtils.join(ConstantManager.URL_DELIMITER, getPlayedBy()));

        return character;
    }

    public Parent convertToParent () {

        Parent parent = new Parent();

        parent.setUrl(getUrl());
        parent.setRemoteId(extractId(getUrl()));
        parent.setHouseId(0L);
        parent.setName(getName());
        parent.setGender(getGender());
        parent.setCulture(getCulture());
        parent.setBorn(getBorn());
        parent.setDied(getDied());
        parent.setFather(getFather());
        parent.setMother(getMother());
        parent.setSpouse(getSpouse());
        if (!getFather().isEmpty()) parent.setFatherId(extractId(getFather()));
        if (!getMother().isEmpty()) parent.setMotherId(extractId(getMother()));

        //due to problem with saving List<String> at greendao, we concatinate all strings together at same field
        parent.setTitles(TextUtils.join(ConstantManager.URL_DELIMITER, getTitles()));
        parent.setAliases(TextUtils.join(ConstantManager.URL_DELIMITER, getAliases()));
        parent.setAllegiances(TextUtils.join(ConstantManager.URL_DELIMITER, getAllegiances()));
        parent.setBooks(TextUtils.join(ConstantManager.URL_DELIMITER, getBooks()));
        parent.setPovBooks(TextUtils.join(ConstantManager.URL_DELIMITER, getPovBooks()));
        parent.setTvSeries(TextUtils.join(ConstantManager.URL_DELIMITER, getTvSeries()));
        parent.setPlayedBy(TextUtils.join(ConstantManager.URL_DELIMITER, getPlayedBy()));

        return parent;
    }

    private long extractId (String url) {
        long result = -1;
        int pos = url.lastIndexOf("/");
        if (pos>=0 && pos+1<url.length()) {
            try {
                result = Long.valueOf(url.substring(pos + 1));
            } catch (Exception e) {
                result = 0;
            }
            return result;
        } else
            return result;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public String getCulture() {
        return culture;
    }

    public String getBorn() {
        return born;
    }

    public String getDied() {
        return died;
    }

    public List<String> getTitles() {
        return titles;
    }

    public List<String> getAliases() {
        return aliases;
    }

    public String getFather() {
        return father;
    }

    public String getMother() {
        return mother;
    }

    public String getSpouse() {
        return spouse;
    }

    public List<String> getAllegiances() {
        return allegiances;
    }

    public List<String> getBooks() {
        return books;
    }

    public List<String> getPovBooks() {
        return povBooks;
    }

    public List<String> getTvSeries() {
        return tvSeries;
    }

    public List<String> getPlayedBy() {
        return playedBy;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void setAliases(List<String> aliases) {
        this.aliases = aliases;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public void setAllegiances(List<String> allegiances) {
        this.allegiances = allegiances;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    public void setPovBooks(List<String> povBooks) {
        this.povBooks = povBooks;
    }

    public void setTvSeries(List<String> tvSeries) {
        this.tvSeries = tvSeries;
    }

    public void setPlayedBy(List<String> playedBy) {
        this.playedBy = playedBy;
    }
}

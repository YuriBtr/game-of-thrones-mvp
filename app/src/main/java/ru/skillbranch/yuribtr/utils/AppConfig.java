package ru.skillbranch.yuribtr.utils;

public interface AppConfig {

    String BASE_URL = "http://anapioficeandfire.com/api/";
    int START_DELAY = 3000;
    int MAX_CONNECT_TIMEOUT = 2000;
    int MAX_READ_TIMEOUT = 2000;
    int CACHE_TIME = 86400000;//86400000 = 1 day
}

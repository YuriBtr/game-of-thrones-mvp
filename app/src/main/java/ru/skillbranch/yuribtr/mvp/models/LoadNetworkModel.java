package ru.skillbranch.yuribtr.mvp.models;

import android.content.Context;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.data.network.res.CharacterRes;
import ru.skillbranch.yuribtr.data.network.res.HouseRes;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.data.storage.models.CharacterDao;
import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.HouseDao;
import ru.skillbranch.yuribtr.data.storage.models.Parent;
import ru.skillbranch.yuribtr.data.storage.models.ParentDao;
import ru.skillbranch.yuribtr.mvp.presenters.INotifierPresenter;
import ru.skillbranch.yuribtr.utils.AppConfig;
import ru.skillbranch.yuribtr.utils.NetworkStatusChecker;

public class LoadNetworkModel implements ILoadModel{
    private Context mContext;
    private DataManager mDataManager;
    private DaoSession mDaoSession;
    private HouseDao mHouseDao;
    private CharacterDao mCharacterDao;
    private ParentDao mParentDao;
    private List<Character> mCharacterList;
    private List<Parent> mParentList;
    private List<House> mHouseList;
    private INotifierPresenter mLoadPresenter;
    private List<String> mParentsUrlList;
    private int characterRequestsSent = 0, houseRequestsSent = 0;

    //sets preferred house numbers, and their q-ty (also this might be random)
    private long[] mHousesNumbers = {378, 229, 362};

    public LoadNetworkModel(INotifierPresenter loadPresenter) {
        mDataManager = DataManager.getInstance();
        mContext = mDataManager.getContext();
        mCharacterList = new ArrayList<>();
        mParentList = new ArrayList<>();
        mHouseList = new ArrayList<>();
        mParentsUrlList = new ArrayList<>();
        //setting ownership of this loadModel
        mLoadPresenter = loadPresenter;
    }

    //region=================INTERFACE METHODS=================

    @Override
    public void LoadFromNetwork() {
        //if request was made less than 1 day, no loading will be done
        if (System.currentTimeMillis()-mDataManager.getPreferencesManager().getRefreshTime()< AppConfig.CACHE_TIME) {
            imitateLoading();
            return;
        }
        //exit if no network found
        if (!isNetworkPresence()) return;
        for (long houseNumber: mHousesNumbers) {
            getHouse(houseNumber);
        }
    }

    @Override
    public Context getContext() {
        return mContext;
    }

    //endregion

    //region=================INTERNAL METHODS=================
    private boolean isNetworkPresence(){
        return NetworkStatusChecker.isNetworkAvailable(mContext);
    }

    private void imitateLoading() {
        class WaitSplash extends AsyncTask<Void, Void, Void> {
            protected Void doInBackground(Void... params) {
                try {
                    Thread.currentThread();
                    Thread.sleep(AppConfig.START_DELAY);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
                mLoadPresenter.onLoadNetworkFinished();
            }
        }
        WaitSplash waitSplash = new WaitSplash();
        waitSplash.execute();
    }

    private void getHouse(Long id) {
        //Thread.sleep(100);
        houseRequestsSent++;
        Call<HouseRes> callHouse = mDataManager.getHouse(id);
        callHouse.enqueue(new Callback<HouseRes>() {
            @Override
            public void onResponse(Call<HouseRes> call, Response<HouseRes> response) {
                houseRequestsSent--;
                parseHouse(call, response);
            }
            @Override
            public void onFailure(Call<HouseRes> call, Throwable t) {
                houseRequestsSent--;
                parseHouse(call, t);
            }
        });
    }

    private void parseHouse(Call<HouseRes> call, Response<HouseRes> response){
        if (response.code() != 200) {
            //optionally we may pass here some additional info basing on response.code
            parseHouse(call, new Throwable());
            return;
        }
        House house = response.body().convertToHouse();
        addHouse(house);
        //receive all characters
        for (String swornMembers : response.body().getSwornMembers()) {
            getCharacter(swornMembers);
        }
    }

    private void parseHouse(Call<HouseRes> call, Throwable t){
        //workaround all errors if needed (currently not necessary)
    }

    private void getCharacter(String url) {
        //Thread.sleep(100);
        //try to find indexes of members
        int id = extractId(url);
        if (id>0) {
            //BELOW LINES FOR DEBUG ONLY - TO LIMIT NUMBER OF QUERIES
            //if (!(id==1302 || id==40 || id==270 || id==272  || id==294  || id==136 || id==273 || id==274 || id==275)) return;
            //if (characterRequestsSent>50) return;

            characterRequestsSent++;
            Call<CharacterRes> callCharacter = mDataManager.getCharacter(id);

            callCharacter.enqueue(new Callback<CharacterRes>() {
                @Override
                public void onResponse(Call<CharacterRes> call, Response<CharacterRes> response) {
                    characterRequestsSent--;
                    //mLoadPresenter.onLoadProgress(mContext.getString(R.string.characters_left_mvp)+characterRequestsSent);
                    parseCharacter(call, response);
                }
                @Override
                public void onFailure(Call<CharacterRes> call, Throwable t) {
                    characterRequestsSent--;
                    //mLoadPresenter.onLoadProgress(mContext.getString(R.string.characters_left_mvp)+characterRequestsSent);
                    parseCharacter(call, t);
                }
            });
        }
    }

    private void getParent(String url) {
        //Thread.sleep(100);
        //try to find indexes of members
        int id = extractId(url);
        if (id>0) {
            //BELOW LINE FOR DEBUG ONLY - TO LIMIT NUMBER OF QUERIES
            //if (!(id==1302 || id==40 || id==270 || id==272  || id==294  || id==136 || id==273 || id==274 || id==275)) return;
            characterRequestsSent++;
            Call<CharacterRes> callCharacter = mDataManager.getCharacter(id);

            callCharacter.enqueue(new Callback<CharacterRes>() {
                @Override
                public void onResponse(Call<CharacterRes> call, Response<CharacterRes> response) {
                    characterRequestsSent--;
                    //mLoadPresenter.onLoadProgress(mContext.getString(R.string.characters_left_mvp)+characterRequestsSent);
                    parseParent(call, response);
                }
                @Override
                public void onFailure(Call<CharacterRes> call, Throwable t) {
                    characterRequestsSent--;
                    //mLoadPresenter.onLoadProgress(mContext.getString(R.string.characters_left_mvp)+characterRequestsSent);
                    parseParent(call, t);
                }
            });
        }
    }

    private int extractId (String url) {
        int pos = url.lastIndexOf("/");
        if (pos>=0 && pos+1<url.length()) {
            return Integer.valueOf(url.substring(pos + 1));
        } else return 0;
    }

    private void parseCharacter(Call<CharacterRes> call, Response<CharacterRes> response){
        //if arrived response non-valid
        if (response.code() != 200) {
            //optionally we may pass here some additional info basing on response.code
            parseCharacter(call, new Throwable());
            return;
        }
        Character character = response.body().convertToCharacter();
        addCharacter(character);

        //prevents recurse (parents request their parents) and limit parents level to 1
        if (!character.getFather().isEmpty()) {
            if (!mParentsUrlList.contains(character.getUrl())) {
                mParentsUrlList.add(character.getFather());
                getParent(character.getFather());
            }
        }
        if (!character.getMother().isEmpty()) {
            if (!mParentsUrlList.contains(character.getUrl())) {
                mParentsUrlList.add(character.getMother());
                getParent(character.getMother());
            }
        }
    }

    private void parseParent(Call<CharacterRes> call, Response<CharacterRes> response){
        //if arrived response non-valid
        if (response.code() != 200) {
            //optionally we may pass here some additional info basing on response.code
            parseCharacter(call, new Throwable());
            return;
        }
        Parent parent = response.body().convertToParent();
        addParent(parent);
    }

    private void parseCharacter(Call<CharacterRes> call, Throwable t) {
        //workaround all errors if needed (currently not necessary)
    }

    private void parseParent(Call<CharacterRes> call, Throwable t) {
        //workaround all errors if needed (currently not necessary)
    }


    private void addCharacter(Character character){
        mCharacterList.add(character);
        trySaveToDb();
    }

    private void addParent(Parent parent){
        mParentList.add(parent);
        trySaveToDb();
    }

    private void addHouse(House house){
        mHouseList.add(house);
        trySaveToDb();
    }

    private void trySaveToDb(){
        //mLoadPresenter.onLoadProgress(mContext.getString(R.string.saving_to_db_mvp));
        //if all requests already arrived we save them to base at once
        if (characterRequestsSent!=0 || houseRequestsSent!=0) return;
        mDaoSession = mDataManager.getDaoSession();
        mHouseDao = mDaoSession.getHouseDao();
        mCharacterDao = mDaoSession.getCharacterDao();
        mParentDao = mDaoSession.getParentDao();

        //clear database
        mHouseDao.deleteAll();
        mCharacterDao.deleteAll();
        mParentDao.deleteAll();

        //saving characters for making indexes by internal mechanism of GreenDao
        mParentDao.insertOrReplaceInTx(mParentList);

        for (Character character: mCharacterList) {
            //linking members to house
            for (House house: mHouseList) {
                if (house.getSwornMembers().contains(character.getUrl())) {
                    house.getSwornMembersList().add(character);
                    character.setHouseId(house.getRemoteId());
                    character.setHouseShortName(house.getHouseShortName());
                    character.setHouseWords(house.getWords());
                }
            }
            //linking parents to their childs
            for (Parent parent: mParentList){
                if (parent.getUrl().equals(character.getFather())) character.setFatherPerson(parent);
                else if (parent.getUrl().equals(character.getMother())) character.setMotherPerson(parent);
            }
        }

        mCharacterDao.insertOrReplaceInTx(mCharacterList);
        mHouseDao.insertOrReplaceInTx(mHouseList);

        mHouseDao.detachAll();
        mDataManager.getPreferencesManager().saveRefreshTime(System.currentTimeMillis());
        mLoadPresenter.onLoadNetworkFinished();
    }
    //endregion
}

package ru.skillbranch.yuribtr.data.storage.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.Keep;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Unique;

import java.util.ArrayList;
import java.util.List;
import org.greenrobot.greendao.DaoException;

@Entity(active = true, nameInDb = "HOUSES")
public class House {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Long remoteId;
    
    private String url;

    private String name;

    private String region;

    private String coatOfArms;

    private String words;

    private String houseShortName;

    private String titles;

    private String seats;

    private String currentLord;

    private String heir;

    private String overlord;

    private String founded;

    private String founder;

    private String diedOut;

    private String ancestralWeapons;

    private String cadetBranches;

    private String swornMembers;

    @ToMany(joinProperties = {
            @JoinProperty(name = "remoteId", referencedName = "houseId")
    })
    private List<Character> swornMembersList;

    /** Used for active entity operations. */
    @Generated(hash = 1167916919)
    private transient HouseDao myDao;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;

    @Generated(hash = 1211161118)
    public House(Long id, @NotNull Long remoteId, String url, String name,
            String region, String coatOfArms, String words, String houseShortName,
            String titles, String seats, String currentLord, String heir,
            String overlord, String founded, String founder, String diedOut,
            String ancestralWeapons, String cadetBranches, String swornMembers) {
        this.id = id;
        this.remoteId = remoteId;
        this.url = url;
        this.name = name;
        this.region = region;
        this.coatOfArms = coatOfArms;
        this.words = words;
        this.houseShortName = houseShortName;
        this.titles = titles;
        this.seats = seats;
        this.currentLord = currentLord;
        this.heir = heir;
        this.overlord = overlord;
        this.founded = founded;
        this.founder = founder;
        this.diedOut = diedOut;
        this.ancestralWeapons = ancestralWeapons;
        this.cadetBranches = cadetBranches;
        this.swornMembers = swornMembers;
    }

    @Generated(hash = 389023854)
    public House() {
    }

    public Long getId() {
        return id;
    }

    public Long getRemoteId() {
        return remoteId;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCoatOfArms() {
        return coatOfArms;
    }

    public String getWords() {
        return words;
    }

    public String getHouseShortName() {
        return houseShortName;
    }

    public String getTitles() {
        return titles;
    }

    public String getSeats() {
        return seats;
    }

    public String getCurrentLord() {
        return currentLord;
    }

    public String getHeir() {
        return heir;
    }

    public String getOverlord() {
        return overlord;
    }

    public String getFounded() {
        return founded;
    }

    public String getFounder() {
        return founder;
    }

    public String getDiedOut() {
        return diedOut;
    }

    public String getAncestralWeapons() {
        return ancestralWeapons;
    }

    public String getCadetBranches() {
        return cadetBranches;
    }

    public String getSwornMembers() {
        return swornMembers;
    }
    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1044117593)
    public List<Character> getSwornMembersList() {
        if (swornMembersList == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            CharacterDao targetDao = daoSession.getCharacterDao();
            List<Character> swornMembersListNew = targetDao._queryHouse_SwornMembersList(remoteId);
            synchronized (this) {
                if(swornMembersList == null) {
                    swornMembersList = swornMembersListNew;
                }
            }
        }
        return swornMembersList;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCoatOfArms(String coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    public void setCurrentLord(String currentLord) {
        this.currentLord = currentLord;
    }

    public void setHeir(String heir) {
        this.heir = heir;
    }

    public void setOverlord(String overlord) {
        this.overlord = overlord;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public void setDiedOut(String diedOut) {
        this.diedOut = diedOut;
    }

    public void setAncestralWeapons(String ancestralWeapons) {
        this.ancestralWeapons = ancestralWeapons;
    }

    public void setCadetBranches(String cadetBranches) {
        this.cadetBranches = cadetBranches;
    }

    public void setSwornMembers(String swornMembers) {
        this.swornMembers = swornMembers;
    }

    public void setSwornMembersList(List<Character> swornMembersList) {
        this.swornMembersList = swornMembersList;
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }

    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }

    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 2079241011)
    public synchronized void resetSwornMembersList() {
        swornMembersList = null;
    }

    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 451323429)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getHouseDao() : null;
    }
}



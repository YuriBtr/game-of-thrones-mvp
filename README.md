This application provides Android code sample of usage "Game of Throns" API from site: http://anapioficeandfire.com
It is second version, using MVP architecture (at least as I understand it)

It was developed as task for learning courses at http://skill-branch.ru


**Features**:

- Loading 3 houses (Starks, Lannisters, Targaryens)

- Loading all characters from these houses

- Loading all parents of characters from these houses

- Caching results at internal DB (default 1 day)


**Used techniques**:

- MVP

- Retrofit2 + OkHTTP3

- JSON REST API

- Greendao ORM

- Stetho
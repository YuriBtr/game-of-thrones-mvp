package ru.skillbranch.yuribtr.data.network.res;

import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.utils.ConstantManager;


public class HouseRes {

    @SerializedName("url")
    @Expose
    private String url;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("coatOfArms")
    @Expose
    private String coatOfArms;
    @SerializedName("words")
    @Expose
    private String words;
    @SerializedName("titles")
    @Expose
    private List<String> titles = new ArrayList<>();
    @SerializedName("seats")
    @Expose
    private List<String> seats = new ArrayList<>();
    @SerializedName("currentLord")
    @Expose
    private String currentLord;
    @SerializedName("heir")
    @Expose
    private String heir;
    @SerializedName("overlord")
    @Expose
    private String overlord;
    @SerializedName("founded")
    @Expose
    private String founded;
    @SerializedName("founder")
    @Expose
    private String founder;
    @SerializedName("diedOut")
    @Expose
    private String diedOut;
    @SerializedName("ancestralWeapons")
    @Expose
    private List<String> ancestralWeapons = new ArrayList<>();
    @SerializedName("cadetBranches")
    @Expose
    private List<String> cadetBranches = new ArrayList<>();
    @SerializedName("swornMembers")
    @Expose
    private List<String> swornMembers = new ArrayList<>();

    public House convertToHouse(){

        House house = new House();

        house.setRemoteId(extractId(getUrl()));
        house.setCoatOfArms(getCoatOfArms());
        house.setCurrentLord(getCurrentLord());
        house.setDiedOut(getDiedOut());
        house.setFounded(getFounded());
        house.setFounder(getFounder());
        house.setHeir(getHeir());
        house.setName(getName());
        house.setOverlord(getOverlord());
        house.setRegion(getRegion());
        house.setUrl(getUrl());
        house.setWords(getWords());

        //due to problem with saving List<String> at greendao, we concatinate all strings together at same field
        house.setAncestralWeapons(TextUtils.join(ConstantManager.URL_DELIMITER, getAncestralWeapons()));
        house.setCadetBranches(TextUtils.join(ConstantManager.URL_DELIMITER, getCadetBranches()));
        house.setSeats(TextUtils.join(ConstantManager.URL_DELIMITER, getSeats()));
        house.setSwornMembers(TextUtils.join(ConstantManager.URL_DELIMITER, getSwornMembers()));
        house.setTitles(TextUtils.join(ConstantManager.URL_DELIMITER, getTitles()));

        house.setSwornMembersList((List<Character>) new ArrayList<Character>());

        house.setHouseShortName("");
        String[] parts = getName().split(" ");
        //boundary -1 needed for passing last comparation, in order to prevent index out of bounds exception
        for (int i=0; i<parts.length-1; i++) {
            if ("House".equalsIgnoreCase(parts[i])) house.setHouseShortName(parts[i+1]);
        }

        return house;
    }

    //todo:insert try catch block and prevent generate null
    private Long extractId (String url) {
        int pos = url.lastIndexOf("/");
        if (pos>=0 && pos+1<url.length()) {
            return Long.valueOf(url.substring(pos + 1));
        } else return null;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    public String getRegion() {
        return region;
    }

    public String getCoatOfArms() {
        return coatOfArms;
    }

    public String getWords() {
        return words;
    }

    public List<String> getTitles() {
        return titles;
    }

    public List<String> getSeats() {
        return seats;
    }

    public String getCurrentLord() {
        return currentLord;
    }

    public String getHeir() {
        return heir;
    }

    public String getOverlord() {
        return overlord;
    }

    public String getFounded() {
        return founded;
    }

    public String getFounder() {
        return founder;
    }

    public String getDiedOut() {
        return diedOut;
    }

    public List<String> getAncestralWeapons() {
        return ancestralWeapons;
    }

    public List<String> getCadetBranches() {
        return cadetBranches;
    }

    public List<String> getSwornMembers() {
        return swornMembers;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setCoatOfArms(String coatOfArms) {
        this.coatOfArms = coatOfArms;
    }

    public void setWords(String words) {
        this.words = words;
    }

    public void setTitles(List<String> titles) {
        this.titles = titles;
    }

    public void setSeats(List<String> seats) {
        this.seats = seats;
    }

    public void setCurrentLord(String currentLord) {
        this.currentLord = currentLord;
    }

    public void setHeir(String heir) {
        this.heir = heir;
    }

    public void setOverlord(String overlord) {
        this.overlord = overlord;
    }

    public void setFounded(String founded) {
        this.founded = founded;
    }

    public void setFounder(String founder) {
        this.founder = founder;
    }

    public void setDiedOut(String diedOut) {
        this.diedOut = diedOut;
    }

    public void setAncestralWeapons(List<String> ancestralWeapons) {
        this.ancestralWeapons = ancestralWeapons;
    }

    public void setCadetBranches(List<String> cadetBranches) {
        this.cadetBranches = cadetBranches;
    }

    public void setSwornMembers(List<String> swornMembers) {
        this.swornMembers = swornMembers;
    }
}


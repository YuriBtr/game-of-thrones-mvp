package ru.skillbranch.yuribtr.mvp.presenters;
//this interface needed to make universal access to owner of LoadNetworkModel from different presenters
public interface INotifierPresenter {
    void onLoadNetworkFinished();
    void onLoadProgress(String message);
}

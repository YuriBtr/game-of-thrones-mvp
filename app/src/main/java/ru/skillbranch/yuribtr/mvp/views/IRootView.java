package ru.skillbranch.yuribtr.mvp.views;

import java.util.List;

import ru.skillbranch.yuribtr.data.storage.models.Character;

public interface IRootView {
    void showMessage (String message);
    void showToast(String message);
    void showError (Throwable e);
    void showLoading ();
    void hideLoading ();
    void assignTabsNames(String tab1, String tab2, String tab3);
    void createAdapter(List<Character> characterList, Class<?> onClickClass);
}

package ru.skillbranch.yuribtr.utils;

import android.os.Handler;
import android.os.Looper;

public class UiUpdater {

    public static void runOnUiThread(Runnable runnable){
        final Handler UIHandler = new Handler(Looper.getMainLooper());
        UIHandler.post(runnable);
    }
}


package ru.skillbranch.yuribtr.mvp.presenters;

import android.content.Context;
import android.support.annotation.Nullable;

import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.mvp.models.ILoadModel;
import ru.skillbranch.yuribtr.mvp.models.LoadNetworkModel;
import ru.skillbranch.yuribtr.mvp.views.ISplashView;
import ru.skillbranch.yuribtr.utils.ConstantManager;

public class SplashPresenter implements ISplashPresenter, INotifierPresenter {
    private static SplashPresenter ourInstance = new SplashPresenter();
    private ISplashView mSplashView;
    private ILoadModel mLoadModel;
    private Context mContext;
    private int mLoadingState = ConstantManager.STATE_IDLE;

    //Singleton
    public static SplashPresenter getInstance(){
        return ourInstance;
    }

    //region=================INTERNAL METHODS=================

    private SplashPresenter() {
        mLoadModel = new LoadNetworkModel(this);//setting ownership of this loadModel
        mContext = mLoadModel.getContext();
        LoadFromNetwork();
    }

    private void UpdateViewState(){
        if (getView()!=null) {
            switch (mLoadingState){
                case ConstantManager.STATE_NETWORK_LOADING:
                    getView().showMessage(mContext.getString(R.string.loading_from_network_mvp));
                    getView().showLoading();
                    break;
                case ConstantManager.STATE_DB_LOADING:
                    getView().showMessage(mContext.getString(R.string.loading_from_db_mvp));
                    getView().showLoading();
                    break;
                case ConstantManager.STATE_IDLE:
                    getView().showMessage(mContext.getString(R.string.loading_finished_mvp));
                    getView().hideLoading();
                    break;
            }
        }
    }

    private void LoadFromNetwork(){
        mLoadingState = ConstantManager.STATE_NETWORK_LOADING;
        UpdateViewState();
        mLoadModel.LoadFromNetwork();
    }

    //endregion

    //region=================INTERFACE METHODS=================

    @Override
    public void takeView(ISplashView splashView) {
        mSplashView = splashView;
    }


    @Override
    public void dropView() {
        mSplashView=null;
    }

    @Override
    public void initView() {
        UpdateViewState();
    }

    @Nullable
    @Override
    public ISplashView getView() {
        return mSplashView;
    }

    @Override
    public void onLoadNetworkFinished() {
        if (getView()!=null) {
            mLoadingState = ConstantManager.STATE_IDLE;
            UpdateViewState();
            mSplashView.gotoNextActivity();
        }
    }

    @Override
    public void onLoadProgress(String message) {
        if (getView()!=null) getView().showMessage(message);
    }

    //endregion
}

package ru.skillbranch.yuribtr.mvp.models;

import java.util.ArrayList;
import java.util.List;

import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.data.storage.models.DaoSession;
import ru.skillbranch.yuribtr.data.storage.models.House;
import ru.skillbranch.yuribtr.data.storage.models.HouseDao;
import ru.skillbranch.yuribtr.mvp.presenters.INotifierPresenter;

public class LoadDbModel {
    private DataManager mDataManager;
    private INotifierPresenter mLoadPresenter;
    private DaoSession mDaoSession;

    public LoadDbModel(INotifierPresenter loadPresenter) {
        mDataManager = DataManager.getInstance();
        mDaoSession = mDataManager.getDaoSession();
        mLoadPresenter = loadPresenter;
    }

    public List<House> getHouses() {
        List<House> houseList = new ArrayList<>();
        try {
            houseList = mDaoSession.queryBuilder(House.class)
                    .orderDesc(HouseDao.Properties.Name)
                    .build()
                    .list();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return houseList;
    }

}

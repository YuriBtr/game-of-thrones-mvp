package ru.skillbranch.yuribtr.data.managers;

import android.content.SharedPreferences;

import ru.skillbranch.yuribtr.utils.ConstantManager;
import ru.skillbranch.yuribtr.utils.App;

public class PreferencesManager {

    private SharedPreferences mSharedPreferences;


    public PreferencesManager() {
        this.mSharedPreferences= App.getSharedPreferences();
    }

    public void saveRefreshTime (long refreshTime){
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putLong(ConstantManager.LAST_REFRESH, refreshTime);
        editor.apply();
    }

    public long getRefreshTime (){
        return mSharedPreferences.getLong(ConstantManager.LAST_REFRESH, 0);
    }


}

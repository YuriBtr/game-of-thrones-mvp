package ru.skillbranch.yuribtr.data.network.interceptors;

import ru.skillbranch.yuribtr.data.managers.DataManager;
import ru.skillbranch.yuribtr.data.managers.PreferencesManager;
import ru.skillbranch.yuribtr.utils.ConstantManager;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class HeaderInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        PreferencesManager pm = DataManager.getInstance().getPreferencesManager();

        Request original = chain.request();

        Request.Builder requestBuilder = original.newBuilder()
                //.header("Accept","text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
                //.header("Accept-Encoding","gzip, deflate")
                //.header("Accept-Language","ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3")
                .header("Cache-Control", "max-age="+ ConstantManager.CACHE_EXPIRE_DEFAULT)
                //.header("Connection","keep-alive")
                //.header("DNT","1")
                .header("Host","anapioficeandfire.com")
                //.header("Upgrade-Insecure-Requests","1")
                .header("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:49.0) Gecko/20100101 Firefox/49.0");

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}

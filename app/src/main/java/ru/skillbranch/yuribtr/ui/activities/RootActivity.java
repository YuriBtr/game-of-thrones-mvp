package ru.skillbranch.yuribtr.ui.activities;

import android.animation.LayoutTransition;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.skillbranch.yuribtr.R;
import ru.skillbranch.yuribtr.data.storage.models.Character;
import ru.skillbranch.yuribtr.data.storage.models.CharacterDTO;
import ru.skillbranch.yuribtr.mvp.presenters.RootPresenter;
import ru.skillbranch.yuribtr.mvp.views.IRootView;
import ru.skillbranch.yuribtr.ui.adapters.CharactersAdapter;
import ru.skillbranch.yuribtr.utils.ConstantManager;
import ru.skillbranch.yuribtr.utils.UiHelper;

public class RootActivity extends BaseActivity implements IRootView {
    RootPresenter mPresenter = RootPresenter.getInstance();
    public static String sTab1 = "UNDEFINED", sTab2 = "UNDEFINED", sTab3 = "UNDEFINED";
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private CharactersAdapter mCharactersAdapter;
    private RecyclerView mRecyclerView;

    @BindView(R.id.main_content)
    CoordinatorLayout mCoordinatorLayout;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.navigation_drawer)
    DrawerLayout mDrawerLayout;

    @BindView(R.id.navigation_view)
    NavigationView mNavigationView;

    @BindView(R.id.container)
    ViewPager mViewPager;

    @BindView(R.id.tabs)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);

        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                switch (tab.getPosition()){
                    case 0:  mNavigationView.getMenu().findItem(R.id.first_tab_menu).setChecked(true);
                        break;
                    case 1:  mNavigationView.getMenu().findItem(R.id.second_tab_menu).setChecked(true);
                        break;
                    case 2:  mNavigationView.getMenu().findItem(R.id.third_tab_menu).setChecked(true);
                        break;
                }
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setupToolbar();
        setupDrawer();

        mPresenter.takeView(this);
        mPresenter.initView();

        LayoutTransition layoutTransition = new LayoutTransition();
        layoutTransition.enableTransitionType(LayoutTransition.CHANGING);
        mNavigationView.setLayoutTransition(layoutTransition);
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }


    private void setupDrawer() {
        mNavigationView.setCheckedItem(R.id.first_tab_menu);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                                                              @Override
                                                              public boolean onNavigationItemSelected(MenuItem item) {
                                                                  if (item.getItemId()==R.id.first_tab_menu) {
                                                                      mViewPager.setCurrentItem(0);
                                                                  } else
                                                                  if (item.getItemId()==R.id.second_tab_menu) {
                                                                      mViewPager.setCurrentItem(1);
                                                                  } else
                                                                  if (item.getItemId()==R.id.third_tab_menu) {
                                                                      mViewPager.setCurrentItem(2);
                                                                  }
                                                                  item.setChecked(true);
                                                                  mDrawerLayout.closeDrawer(GravityCompat.START);
                                                                  return false;
                                                              }
                                                          }
        );

    }


    @Override
    protected void onDestroy() {
        mPresenter.dropView();
        super.onDestroy();
    }

    @Override
    public void assignTabsNames(String tab1, String tab2, String tab3) {
        sTab1 = tab1+"s";
        sTab2 = tab2+"s";
        sTab3 = tab3+"s";
        mSectionsPagerAdapter.notifyDataSetChanged();
        MenuItem menuItem1 = mNavigationView.getMenu().findItem(R.id.first_tab_menu);
        MenuItem menuItem2 = mNavigationView.getMenu().findItem(R.id.second_tab_menu);
        MenuItem menuItem3 = mNavigationView.getMenu().findItem(R.id.third_tab_menu);
        menuItem1.setTitle(sTab1);
        menuItem2.setTitle(sTab2);
        menuItem3.setTitle(sTab3);

        if (menuItem1.getTitle().equals("Starks")) menuItem1.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
        else if (menuItem1.getTitle().equals("Lannisters")) menuItem1.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
        else if (menuItem1.getTitle().equals("Targaryens")) menuItem1.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));

        if (menuItem2.getTitle().equals("Starks")) menuItem2.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
        else if (menuItem2.getTitle().equals("lannisters")) menuItem2.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
        else if (menuItem2.getTitle().equals("Targaryens")) menuItem2.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));

        if (menuItem3.getTitle().equals("Starks")) menuItem3.setIcon(getResources().getDrawable(R.drawable.stark_icon_xxxsmall));
        else if (menuItem3.getTitle().equals("Lannisters")) menuItem3.setIcon(getResources().getDrawable(R.drawable.lanister_icon_xxxsmall));
        else if (menuItem3.getTitle().equals("Targaryens")) menuItem3.setIcon(getResources().getDrawable(R.drawable.targariyen_icon_xxxsmall));
    }

    @Override
    public void showMessage(String message) {
        showSnackbar(message);
    }

    @Override
    public void showError(Throwable e) {
        showSnackbar(e.getMessage());
    }

    @Override
    public void showLoading() {
        showProgress();
    }

    @Override
    public void hideLoading() {
        hideProgress();
    }

    @Override
    public void createAdapter(List<Character> characterList, final Class<?> onClickClass) {
        mCharactersAdapter = new CharactersAdapter(characterList, new CharactersAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(Character item) {
                CharacterDTO characterDTO = item.convertToCharacterDTO();
                Intent profileIntent = new Intent(RootActivity.this, onClickClass);
                profileIntent.putExtra(ConstantManager.PARCELABLE_KEY, characterDTO);
                startActivity(profileIntent);
            }
        });
        mRecyclerView.swapAdapter(mCharactersAdapter, false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public void onBackPressed() {
        //prevent revert to splash screen
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else moveTaskToBack(true);
    }


    private void showSnackbar(String message) {
        Snackbar.make(mCoordinatorLayout, message, Snackbar.LENGTH_LONG).show();
    }


    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber-1);
            fragment.setArguments(args);
            return fragment;
        }
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            RootActivity rootActivity = (RootActivity) getActivity();
            rootActivity.mRecyclerView = (RecyclerView) rootView.findViewById(R.id.character_list);
            //receive an adapter
            RootPresenter.getInstance().onPageSelected(getArguments().getInt(ARG_SECTION_NUMBER));
            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return sTab1;
                case 1:
                    return sTab2;
                case 2:
                    return sTab3;
            }
            return null;
        }
    }



}

package ru.skillbranch.yuribtr.data.storage.models;

import android.os.Parcel;
import android.os.Parcelable;

import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Unique;

public class CharacterDTO implements Parcelable {

    @Id
    private Long id;

    @NotNull
    @Unique
    private Long remoteId;

    private Long houseId;

    private String houseShortName;

    private String url;

    private String name;

    private String gender;

    private String culture;

    private String born;

    private String died;

    private String titles;

    private String aliases;

    private String father;

    private String mother;

    private Long fatherId;

    private CharacterDTO fatherCharacter;

    private Long motherId;

    private CharacterDTO motherCharacter;

    private String spouse;

    private String allegiances;

    private String books;

    private String povBooks;

    private String tvSeries;

    private String playedBy;

    private String houseWords;

    public CharacterDTO() {
    }

    protected CharacterDTO(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readLong();
        remoteId = in.readByte() == 0x00 ? null : in.readLong();
        houseId = in.readByte() == 0x00 ? null : in.readLong();
        houseShortName = in.readString();
        url = in.readString();
        name = in.readString();
        gender = in.readString();
        culture = in.readString();
        born = in.readString();
        died = in.readString();
        titles = in.readString();
        aliases = in.readString();
        father = in.readString();
        mother = in.readString();
        fatherId = in.readByte() == 0x00 ? null : in.readLong();
        fatherCharacter = (CharacterDTO) in.readValue(CharacterDTO.class.getClassLoader());
        motherId = in.readByte() == 0x00 ? null : in.readLong();
        motherCharacter = (CharacterDTO) in.readValue(CharacterDTO.class.getClassLoader());
        spouse = in.readString();
        allegiances = in.readString();
        books = in.readString();
        povBooks = in.readString();
        tvSeries = in.readString();
        playedBy = in.readString();
        houseWords = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(id);
        }
        if (remoteId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(remoteId);
        }
        if (houseId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(houseId);
        }
        dest.writeString(houseShortName);
        dest.writeString(url);
        dest.writeString(name);
        dest.writeString(gender);
        dest.writeString(culture);
        dest.writeString(born);
        dest.writeString(died);
        dest.writeString(titles);
        dest.writeString(aliases);
        dest.writeString(father);
        dest.writeString(mother);
        if (fatherId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(fatherId);
        }
        dest.writeValue(fatherCharacter);
        if (motherId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(motherId);
        }
        dest.writeValue(motherCharacter);
        dest.writeString(spouse);
        dest.writeString(allegiances);
        dest.writeString(books);
        dest.writeString(povBooks);
        dest.writeString(tvSeries);
        dest.writeString(playedBy);
        dest.writeString(houseWords);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CharacterDTO> CREATOR = new Parcelable.Creator<CharacterDTO>() {
        @Override
        public CharacterDTO createFromParcel(Parcel in) {
            return new CharacterDTO(in);
        }

        @Override
        public CharacterDTO[] newArray(int size) {
            return new CharacterDTO[size];
        }
    };

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRemoteId() {
        return remoteId;
    }

    public void setRemoteId(Long remoteId) {
        this.remoteId = remoteId;
    }

    public Long getHouseId() {
        return houseId;
    }

    public void setHouseId(Long houseId) {
        this.houseId = houseId;
    }

    public String getHouseShortName() {
        return houseShortName;
    }

    public void setHouseShortName(String houseShortName) {
        this.houseShortName = houseShortName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCulture() {
        return culture;
    }

    public void setCulture(String culture) {
        this.culture = culture;
    }

    public String getBorn() {
        return born;
    }

    public void setBorn(String born) {
        this.born = born;
    }

    public String getDied() {
        return died;
    }

    public void setDied(String died) {
        this.died = died;
    }

    public String getTitles() {
        return titles;
    }

    public void setTitles(String titles) {
        this.titles = titles;
    }

    public String getAliases() {
        return aliases;
    }

    public void setAliases(String aliases) {
        this.aliases = aliases;
    }

    public String getFather() {
        return father;
    }

    public void setFather(String father) {
        this.father = father;
    }

    public String getMother() {
        return mother;
    }

    public void setMother(String mother) {
        this.mother = mother;
    }

    public Long getFatherId() {
        return fatherId;
    }

    public void setFatherId(Long fatherId) {
        this.fatherId = fatherId;
    }

    public CharacterDTO getFatherCharacter() {
        return fatherCharacter;
    }

    public void setFatherCharacter(CharacterDTO fatherCharacter) {
        this.fatherCharacter = fatherCharacter;
    }

    public Long getMotherId() {
        return motherId;
    }

    public void setMotherId(Long motherId) {
        this.motherId = motherId;
    }

    public CharacterDTO getMotherCharacter() {
        return motherCharacter;
    }

    public void setMotherCharacter(CharacterDTO motherCharacter) {
        this.motherCharacter = motherCharacter;
    }

    public String getSpouse() {
        return spouse;
    }

    public void setSpouse(String spouse) {
        this.spouse = spouse;
    }

    public String getAllegiances() {
        return allegiances;
    }

    public void setAllegiances(String allegiances) {
        this.allegiances = allegiances;
    }

    public String getBooks() {
        return books;
    }

    public void setBooks(String books) {
        this.books = books;
    }

    public String getPovBooks() {
        return povBooks;
    }

    public void setPovBooks(String povBooks) {
        this.povBooks = povBooks;
    }

    public String getTvSeries() {
        return tvSeries;
    }

    public void setTvSeries(String tvSeries) {
        this.tvSeries = tvSeries;
    }

    public String getPlayedBy() {
        return playedBy;
    }

    public void setPlayedBy(String playedBy) {
        this.playedBy = playedBy;
    }

    public String getHouseWords() {
        return houseWords;
    }

    public void setHouseWords(String houseWords) {
        this.houseWords = houseWords;
    }
}